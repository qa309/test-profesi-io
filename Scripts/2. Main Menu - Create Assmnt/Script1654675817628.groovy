import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.delay(2)

WebUI.click(findTestObject('Main Menu/Mainmenu_buatassemen_button'))

WebUI.setText(findTestObject('Main Menu/buat_assmnt_judul_texbox'), 'test create')

WebUI.delay(1)

WebUI.click(findTestObject('Main Menu/buat_assmnt_tglmulai_texbox'))

WebUI.click(findTestObject('Main Menu/buat_assmnt_tglmulai_0106_date'))

WebUI.delay(1)

WebUI.click(findTestObject('Main Menu/buat_assmnt_tglakhir_texbox'))

WebUI.click(findTestObject('Main Menu/buat_assmnt_tglmulai_1006_date'))

WebUI.delay(1)

WebUI.setText(findTestObject('Main Menu/input_daftarjabatan_texbox'), 'sdm')

WebUI.delay(1)

WebUI.click(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/button_Tambah_list7'))

WebUI.delay(1)

WebUI.click(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/input_Atasan_'))

WebUI.click(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/div_Lionel Messi                               Kepala Bagian Pengelolaan SDM'))

WebUI.click(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/Bobot Asesor_dropdown'))

WebUI.setText(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/input_Atasan_'), '80')

WebUI.setText(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/input_Bawahan_'), '0')

WebUI.setText(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/input_Rekan Kerja_'), '0')

WebUI.click(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/Bobot Kompetensi_dropdown'))

WebUI.setText(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/input_Kompetensi Inti_'), '70')

WebUI.setText(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/input_Kompetensi Kepemimpinan'), '0')

WebUI.click(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/button_Tambah'))

WebUI.delay(2)

WebUI.click(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/button_Publikasi'))

WebUI.click(findTestObject('Main Menu/Page_Profesi.io/Page_Profesi.io/konfirmasi_ya_button'))

