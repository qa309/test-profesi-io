<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Bobot Asesor_dropdown</name>
   <tag></tag>
   <elementGuidId>b5bdb020-8687-4cd8-a522-7b5cb6fa99c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='(Harus bernilai 100)'])[1]/following::header[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>header</value>
      <webElementGuid>3da9c927-3de7-410b-9186-dc58795976ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>vs-collapse-item--header</value>
      <webElementGuid>3431eeaf-06b6-4957-ba76-6797256023a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bobot Asesorkeyboard_arrow_down</value>
      <webElementGuid>bf1c523b-3798-40e7-a4b1-a03f97820084</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;vuesax-app-is-ltr&quot;]/body[@class=&quot;theme-semi-dark&quot;]/div[@class=&quot;vs-component con-vs-popup popup popup-container vs-popup-primary&quot;]/div[@class=&quot;vs-popup&quot;]/div[@class=&quot;vs-popup--content&quot;]/div[@class=&quot;modal-add-asse&quot;]/div[@class=&quot;collapse-section mt-8&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;vs-collapse pt-2 default&quot;]/div[@class=&quot;vs-collapse-item&quot;]/header[@class=&quot;vs-collapse-item--header&quot;]</value>
      <webElementGuid>bc8215d3-6a97-46e5-b8f3-9f765961051b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(Harus bernilai 100)'])[1]/following::header[1]</value>
      <webElementGuid>40f9b08a-66f1-468c-9a0e-44708ea7dc21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[3]/div/div/header</value>
      <webElementGuid>b05c39af-4ded-4e4d-a999-e357948d02fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//header[(text() = 'Bobot Asesorkeyboard_arrow_down' or . = 'Bobot Asesorkeyboard_arrow_down')]</value>
      <webElementGuid>9d598bab-97d4-4609-b3be-8a5ce7e4bebc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
