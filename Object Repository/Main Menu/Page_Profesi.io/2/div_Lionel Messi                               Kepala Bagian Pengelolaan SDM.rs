<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Lionel Messi                               Kepala Bagian Pengelolaan SDM</name>
   <tag></tag>
   <elementGuidId>e5a085ea-714a-48c2-92ef-5495bb0d243f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Staf Perencanaan SDM'])[2]/following::div[6]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>35f86c7b-32df-4d31-9508-5e838d237657</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>vx-card__body</value>
      <webElementGuid>8d00ddb6-89bb-4e5a-b83e-4de0017af11f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Lionel Messi 
                              Kepala Bagian Pengelolaan SDM
                            </value>
      <webElementGuid>615e067d-04c8-40e2-b788-f330c699ea2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;vuesax-app-is-ltr&quot;]/body[@class=&quot;theme-semi-dark&quot;]/div[@class=&quot;vs-component con-vs-popup popup popup-container vs-popup-primary&quot;]/div[@class=&quot;vs-popup&quot;]/div[@class=&quot;vs-popup--content&quot;]/div[@class=&quot;modal-add-asse&quot;]/div[@class=&quot;collapse-section mt-8&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;vs-collapse pt-2 default&quot;]/div[@class=&quot;vs-collapse-item open-item&quot;]/div[@class=&quot;vs-collapse-item--content&quot;]/div[@class=&quot;con-content--item&quot;]/div[@class=&quot;vx-row&quot;]/div[@class=&quot;vx-col w-1/2&quot;]/ul[@class=&quot;checkbox-list&quot;]/div[@class=&quot;vue-recycle-scroller recycle-scroller ready direction-vertical&quot;]/div[@class=&quot;vue-recycle-scroller__item-wrapper&quot;]/div[@class=&quot;vue-recycle-scroller__item-view&quot;]/li[@class=&quot;item&quot;]/div[@class=&quot;vx-card no-shadow card-border&quot;]/div[@class=&quot;vx-card__collapsible-content vs-con-loading__container&quot;]/div[@class=&quot;vx-card__body&quot;]</value>
      <webElementGuid>3b1461e3-4053-4304-81cd-05abca192cce</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Staf Perencanaan SDM'])[2]/following::div[6]</value>
      <webElementGuid>ac3f0f64-625e-414c-8b8e-ccdfc03292c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Staf Perencanaan SDM'])[1]/following::div[13]</value>
      <webElementGuid>b6b61f96-5068-4a56-be36-795158941437</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Staf Pengelolaan SDM'])[1]/preceding::div[4]</value>
      <webElementGuid>57dddb2a-d397-43f4-8a10-aad05e3d5904</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/li/div/div/div</value>
      <webElementGuid>9ec3b1b3-97de-4f29-8e31-63e9f87dc998</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Lionel Messi 
                              Kepala Bagian Pengelolaan SDM
                            ' or . = 'Lionel Messi 
                              Kepala Bagian Pengelolaan SDM
                            ')]</value>
      <webElementGuid>a0e106d4-aef1-4a91-a9b6-621fba15b7de</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
