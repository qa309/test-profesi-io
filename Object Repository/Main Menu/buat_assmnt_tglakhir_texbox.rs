<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>buat_assmnt_tglakhir_texbox</name>
   <tag></tag>
   <elementGuidId>8fcdabe4-65e4-4b99-9971-6f5e7309c309</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/div/div/div[3]/div[3]/div/div/div[3]/div[2]/div/div[2]/div[1]/div/form/div/div[1]/div[2]/div[2]/div/div/div[1]/input</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>09b7de36-1899-4aad-9530-81749f71533f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>c9f53a65-f435-4b8f-b047-5caa2552c7e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Pilih Tanggal Berakhir</value>
      <webElementGuid>fe4e8cbe-a863-44ed-81e2-d5f65f2ba58f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>disabled</name>
      <type>Main</type>
      <value>disabled</value>
      <webElementGuid>18a19ec1-d893-487e-9d2a-461348269d92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>readonly</name>
      <type>Main</type>
      <value>readonly</value>
      <webElementGuid>af04a471-1c43-487f-bed7-e2d509b2002c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>23b00b5e-28fe-4653-ae88-cea7f2724d66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content-area&quot;)/div[@class=&quot;content-wrapper&quot;]/div[@class=&quot;router-view&quot;]/div[@class=&quot;router-content&quot;]/div[@class=&quot;content-area__content&quot;]/div[2]/div[@class=&quot;fullscreen-wrapper&quot;]/div[@class=&quot;vx-card&quot;]/div[@class=&quot;vx-card__collapsible-content vs-con-loading__container&quot;]/div[@class=&quot;vx-card__body&quot;]/form[@class=&quot;container container-min mt-6&quot;]/div[@class=&quot;vx-row&quot;]/div[@class=&quot;vx-col w-full&quot;]/div[@class=&quot;vx-row mt-4&quot;]/div[@class=&quot;vx-col w-full md:w-1/2 mt-4 md:mt-0&quot;]/div[1]/div[@class=&quot;vdp-datepicker&quot;]/div[1]/input[1]</value>
      <webElementGuid>33322d61-4542-42d9-8a18-1da220276206</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[3]</value>
      <webElementGuid>ebb17a60-87c6-4b80-b60a-74211f5477bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content-area']/div[3]/div/div/div[3]/div[2]/div/div[2]/div/div/form/div/div/div[2]/div[2]/div/div/div/input</value>
      <webElementGuid>55a67c7a-bf16-4471-a510-d07db049bfff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/input</value>
      <webElementGuid>0788ede1-7187-4336-9604-0ddf935184cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @placeholder = 'Pilih Tanggal Berakhir']</value>
      <webElementGuid>9a2d26b9-9401-4972-9f5a-4b5c48096c91</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
